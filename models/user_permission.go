package models

import (
	"github.com/google/uuid"
	"time"
)

type UserPermission struct {
	UserID         uuid.UUID `gorm:"primaryKey"`
	PermissionName string    `gorm:"primaryKey"`
	ExpiresAt      time.Time `json:"expiresAt"`
}
