package models

import (
	"github.com/google/uuid"
	"time"
)

type VerificationCode struct {
	ID        uint      `gorm:"primaryKey"`
	CodeHash  string    `json:"-"`
	UserID    uuid.UUID `json:"-"`
	User      User      `json:"-"`
	CreatedAt time.Time `json:"createdAt"`
}
