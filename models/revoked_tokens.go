package models

import (
	"github.com/google/uuid"
	"time"
)

type RevokedTokens struct {
	TokenID     uuid.UUID `gorm:"primaryKey"`
	TokenExpiry time.Time
}
