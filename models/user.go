package models

import (
	"github.com/google/uuid"
	"time"
)

type User struct {
	ID            uuid.UUID        `gorm:"type:uuid;default:gen_random_uuid()"`
	Email         string           `json:"email" gorm:"uniqueIndex"`
	EmailVerified bool             `json:"emailVerified" gorm:"default:false"`
	Name          string           `json:"name"`
	PasswordHash  string           `json:"-"`
	TosAccepted   bool             `json:"-"`
	Permissions   []UserPermission `json:"-"`
	CreatedAt     time.Time        `json:"createdAt"`
	UpdatedAt     time.Time        `json:"UpdatedAt"`
}
