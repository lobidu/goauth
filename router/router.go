package router

import (
	"github.com/gin-gonic/gin"
	"goauth/controller"
	"goauth/middleware"
	"net/http"
)

func RegisterRoutes(r *gin.Engine) {
	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"ok": true})
	})

	/* Authentication Routes */
	r.POST("/auth/key", controller.GetPublicKey)
	r.POST("/auth/token", middleware.ValidateLogin, controller.GetTokens)
	r.POST("/auth/refresh", middleware.ValidateRefreshToken, controller.GetTokens)
	r.GET("/auth/verify", controller.VerifyEmail)
	r.POST("/auth/resend-verification-mail", controller.SendVerificationMail)
	r.POST("/auth/reset-request", controller.RequestPasswordReset)
	r.POST("/auth/reset", controller.ResetPassword)
	r.PUT("/auth/password", middleware.Authenticate, controller.UpdatePassword)
	r.DELETE("/auth/token", middleware.Authenticate, controller.RevokeToken)

	/* User routes */
	r.POST("/user", controller.CreateUser)
	userRouter := r.Group("/user")
	userRouter.Use(middleware.Authenticate)
	{
		userRouter.GET("/:id", controller.FindUser)
		userRouter.PATCH("/:id", controller.UpdateUser)
		userRouter.DELETE("/:id", controller.DeleteUser)
	}

	thingRouter := r.Group("/thing", middleware.Authenticate)
	{
		thingRouter.POST("/", middleware.Authorize("create:thing"), controller.CreateThing)
		thingRouter.GET("/:id", middleware.Authorize("read:thing"), controller.FindThing)
		thingRouter.PATCH("/:id", middleware.Authorize("update:thing"), controller.UpdateThing)
		thingRouter.DELETE("/:id", middleware.Authorize("update:thing"), controller.DeleteThing)
	}
}
