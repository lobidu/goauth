# Goauth

This is an application that runs a simple user authentication server

## Prerequisites
You need the following technologies to run this:
- Go version >= 1.18
- Docker
- Make
- A working SMTP server to send the verification mails

## Get started
1. After cloning this repository run:
   ```shell
   make setup
   ```
   this will:
   - create JWT encryption keys
   - create a Postgres DB in Docker using a random password
   - generate a `.env` file to customize

2. Update the `.env` file with the correct values from your environment
3. Run `go build goauth`

## What this code is missing

For the purpose of demonstration this example is missing the critical cronjob
that deletes expired entries from the `verification_codes` and `revoked_tokens` tables. When using in production utilize your preferred cronjob method
to delete them.

Also the code revocation block has a bug that I wasn't able to figure out before the bit summit. That needs to be overhauled. It doesn't check properly whether or not a token is revoked.