POSTGRES_PASSWORD := $(shell openssl rand -hex 12)

keys:
	ssh-keygen -t ecdsa -b 521 -m PKCS8 -N "" -f .keys/ecdsa-p521; \
    openssl ec -in .keys/ecdsa-p521 -pubout > .keys/ecdsa-p521-public.pem

db:
	sed -i '' "s;^POSTGRES_PASSWORD=.*;POSTGRES_PASSWORD=$(POSTGRES_PASSWORD);g" .env; \
	docker run --name goauth-db -e POSTGRES_PASSWORD=$(POSTGRES_PASSWORD) -e POSTGRES_USER=goauth -p 5432:5432 -d postgres

env:
	cp .env.sample .env; \
	sed -i '' "s;/path/to;$(shell pwd);g" .env; \


setup:
	make keys && make env && make db

run:
	go build