package userservice

import (
	"goauth/common"
	"goauth/models"
	"time"
)

func AddPermission(user *models.User, permission string, validUntil time.Time) error {
	userPermission := models.UserPermission{
		UserID:         user.ID,
		PermissionName: permission,
		ExpiresAt:      validUntil,
	}

	// TODO: Handle conflicts
	if err := common.DB.Save(&userPermission).Error; err != nil {
		return err
	}

	return nil
}
