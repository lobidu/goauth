package userservice

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"goauth/common"
	"goauth/models"
	"goauth/services/mailservice"
	"golang.org/x/crypto/bcrypt"
	"log"
)

func VerifyEmail(user *models.User) {
	verificationCode := common.GenerateRandomString(12)
	hashPayload := fmt.Sprintf("%s.%s", user.Email, verificationCode)
	verificationCodeHash, err := bcrypt.GenerateFromPassword(
		[]byte(hashPayload),
		bcrypt.DefaultCost,
	)
	if err != nil {
		log.Printf("Unable to generate Hash from Email verification code: %s", err)
	}

	code := models.VerificationCode{
		CodeHash: string(verificationCodeHash),
		User:     *user,
	}

	common.DB.Create(&code)

	token := fmt.Sprintf("%d.%s", code.ID, verificationCode)

	mailservice.SendEmailVerificationMail(mailservice.EmailVerificationMail{
		To:                user.Email,
		VerificationToken: token,
	})
}

func GetUserFromContext(c *gin.Context) (*models.User, error) {
	var user models.User

	userId, ok := c.Get("userId")

	if !ok {
		return nil, fmt.Errorf("no user ID in context")
	}

	if err := common.DB.Where("id = ?", userId.(uuid.UUID).String()).First(&user).
		Error; err != nil {
		return nil, err
	}

	return &user, nil
}

func FindById(id string) (*models.User, error) {
	var user models.User

	if err := common.DB.Where("id = ?", id).First(&user).Error; err != nil {
		return nil, errors.New("user not found")
	}

	return &user, nil
}
