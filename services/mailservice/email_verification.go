package mailservice

import (
	"fmt"
	"log"
	"os"
)

type VerifyEmailTemplateData struct {
	Token string
	Email string
	Url   string
}

type EmailVerificationMail struct {
	To                string
	VerificationToken string
}

func SendEmailVerificationMail(verificationMailData EmailVerificationMail) {
	mailData := &Mail{
		To:       verificationMailData.To,
		Subject:  "Please Verify Your Email",
		Template: "verify_email.html",
		BodyData: &VerifyEmailTemplateData{
			Email: verificationMailData.To,
			Token: verificationMailData.VerificationToken,
			Url:   fmt.Sprintf("%s/auth/verify", os.Getenv("EXTERNAL_URL")),
		},
	}

	err := Send(mailData)

	if err != nil {
		log.Printf("Error trying to send Verification Email: %s", err)
		if os.Getenv("DEBUG_MODE") == "true" {
			// In Debug mode, log the token to be able to proceed without Mail being set up
			log.Printf("Undelivered Token: %s", verificationMailData.VerificationToken)
		}
	}
}
