package mailservice

import (
	"goauth/models"
	"log"
	"os"
)

type PasswordResetTemplateData struct {
	Token  string
	UserID string
	Url    string
}

type PasswordResetMail struct {
	User              *models.User
	VerificationToken string
}

func SendPasswordResetMail(verificationMailData PasswordResetMail) {
	mailData := &Mail{
		To:       verificationMailData.User.Email,
		Subject:  "Password Reset",
		Template: "password_reset.html",
		BodyData: &PasswordResetTemplateData{
			UserID: verificationMailData.User.ID.String(),
			Token:  verificationMailData.VerificationToken,
			Url:    os.Getenv("RESET_PASSWORD_URL"),
		},
	}

	err := Send(mailData)

	if err != nil {
		log.Printf("Error trying to send Password reset Email: %s", err)
		if os.Getenv("DEBUG_MODE") == "true" {
			// In Debug mode, log the token to be able to proceed without Mail being set up
			log.Printf("Undelivered Token: %s", verificationMailData.VerificationToken)
		}
	}
}
