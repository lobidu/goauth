package mailservice

import (
	"bytes"
	"embed"
	mail "github.com/xhit/go-simple-mail/v2"
	"html/template"
	"io/fs"
	"log"
	"os"
)

type Mail struct {
	To       string
	Subject  string
	Template string
	BodyData any
}

const (
	templatesDir = "templates"
)

var (
	//go:embed templates/*
	files     embed.FS
	templates map[string]*template.Template
)

func loadTemplates() error {
	if templates == nil {
		templates = make(map[string]*template.Template)
	}
	tmplFiles, err := fs.ReadDir(files, templatesDir)
	if err != nil {
		return err
	}

	for _, tmpl := range tmplFiles {
		if tmpl.IsDir() {
			continue
		}

		pt, err := template.ParseFS(files, templatesDir+"/"+tmpl.Name())
		if err != nil {
			return err
		}

		templates[tmpl.Name()] = pt
	}
	return nil
}

func Send(mailData *Mail) error {
	if templates == nil {
		err := loadTemplates()
		if err != nil {
			log.Printf("Unable to load Message templates: %s", err)
		}
	}

	email := mail.NewMSG()
	email.SetFrom(os.Getenv("SMTP_FROM_ADDRESS"))
	email.AddTo(mailData.To)
	email.SetSubject(mailData.Subject)

	bodyTemplate := templates[mailData.Template]

	buf := new(bytes.Buffer)
	err := bodyTemplate.Execute(buf, mailData.BodyData)
	if err != nil {
		log.Printf("Unable to build Message template: %s", err)
	}

	email.SetBody(mail.TextHTML, buf.String())

	client, err := GetSMTPClient()

	if err != nil {
		return err
	}

	return email.Send(client)
}
