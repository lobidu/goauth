package tokenservice

import (
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
	"goauth/common"
	"goauth/models"
	"time"
)

type RevocableToken struct {
	UserId string `json:"user_id"`
	jwt.RegisteredClaims
}

type AuthenticationTokenClaim struct {
	UserId      string   `json:"user_id"`
	Permissions []string `json:"permissions"`
	jwt.RegisteredClaims
}

type RefreshTokenClaim struct {
	UserId string `json:"user_id"`
	jwt.RegisteredClaims
}

type IdTokenClaim struct {
	UserId string `json:"user_id"`
	Name   string `json:"name"`
	Email  string `json:"email"`
	jwt.RegisteredClaims
}

func GenerateAuthenticationToken(userId string, permissions []string) (error, string) {
	claim := &AuthenticationTokenClaim{
		userId,
		permissions,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(24 * time.Hour)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			NotBefore: jwt.NewNumericDate(time.Now()),
			Issuer:    issuer,
			ID:        uuid.New().String(),
		},
	}

	token := jwt.NewWithClaims(signingAlg, claim)
	key := GetPrivateKey()

	tokenString, err := token.SignedString(key)

	return err, tokenString
}

func GenerateRefreshToken(userId string) (error, string) {
	claim := &RefreshTokenClaim{
		userId,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(60 * 24 * time.Hour)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			NotBefore: jwt.NewNumericDate(time.Now()),
			Issuer:    issuer,
			ID:        uuid.New().String(),
		},
	}

	token := jwt.NewWithClaims(signingAlg, claim)
	key := GetPrivateKey()

	tokenString, err := token.SignedString(key)

	return err, tokenString
}

func GenerateIdToken(user *models.User) (error, string) {
	claim := &IdTokenClaim{
		user.ID.String(),
		user.Name,
		user.Email,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(24 * time.Hour)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			Issuer:    issuer,
		},
	}

	token := jwt.NewWithClaims(signingAlg, claim)
	key := GetPrivateKey()

	tokenString, err := token.SignedString(key)

	return err, tokenString
}

func ValidateAuthenticationToken(tokenString string) (error, *AuthenticationTokenClaim) {
	token, err := jwt.ParseWithClaims(
		tokenString,
		&AuthenticationTokenClaim{},
		keyFunc,
	)

	if err != nil {
		return err, nil
	}

	claims, ok := token.Claims.(*AuthenticationTokenClaim)

	if !(ok && token.Valid) {
		return fmt.Errorf("invalid token"), nil
	}

	var revokedToken models.RevokedTokens
	result := common.DB.Where("token_id = ?", claims.ID).First(&revokedToken)
	if result.Error != nil {
		// This could be more expressive, but I'm running short on time.
		return errors.New("invalid token"), nil
	}

	return nil, claims

}

func ValidateRefreshToken(tokenString string) (error, *RefreshTokenClaim) {
	token, err := jwt.ParseWithClaims(
		tokenString,
		&RefreshTokenClaim{},
		keyFunc,
	)

	if err != nil {
		return err, nil
	}

	claims, ok := token.Claims.(*RefreshTokenClaim)

	if !(ok && token.Valid) {
		return fmt.Errorf("invalid token"), nil
	}

	var revokedToken models.RevokedTokens
	result := common.DB.Where("token_id = ?", claims.ID).First(&revokedToken)
	if result.Error != nil {
		// This could be more expressive, but I'm running short on time.
		return errors.New("invalid token"), nil
	}

	return nil, claims
}

func ValidateTokenToBeRevoked(tokenString string) (error, *RevocableToken) {
	token, err := jwt.ParseWithClaims(
		tokenString,
		&RevocableToken{},
		keyFunc,
	)

	if err != nil {
		return err, nil
	}

	claims, ok := token.Claims.(*RevocableToken)

	if !(ok && token.Valid) {
		return fmt.Errorf("invalid token"), nil
	}

	var revokedToken models.RevokedTokens
	result := common.DB.Where("token_id = ?", claims.ID).First(&revokedToken)
	if result.Error != nil {
		// This could be more expressive, but I'm running short on time.
		return errors.New("invalid token"), nil
	}

	return nil, claims
}
