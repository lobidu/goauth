package tokenservice

import (
	"crypto/ecdsa"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"log"
	"os"
)

var issuer = os.Getenv("JWT_ISSUER_ID")
var signingAlg = jwt.SigningMethodES512

func keyFunc(token *jwt.Token) (interface{}, error) {

	if _, ok := token.Method.(*jwt.SigningMethodECDSA); !ok {
		return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
	}

	return GetPublicKey()
}

func GetPrivateKey() *ecdsa.PrivateKey {
	key, _ := os.ReadFile(os.Getenv("JWT_SIGNING_KEY_PATH"))

	var ecdsaKey *ecdsa.PrivateKey
	ecdsaKey, err := jwt.ParseECPrivateKeyFromPEM(key)
	if err != nil {
		log.Fatalf("Unable to parse ECDSA private key: %v", err)
	}
	return ecdsaKey
}

func GetPublicKey() (*ecdsa.PublicKey, error) {
	key, _ := os.ReadFile(os.Getenv("JWT_SIGNING_PUBLIC_KEY_PATH"))

	return jwt.ParseECPublicKeyFromPEM(key)
}
