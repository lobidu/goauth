package middleware

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func Authorize(requiredPermission string) func(c *gin.Context) {
	return func(c *gin.Context) {
		data, ok := c.Get("permissions")
		permissions := data.([]string)

		if ok {
			for _, userPermission := range permissions {
				if userPermission == requiredPermission {
					c.Next()
					return
				}
			}
		}

		c.JSON(http.StatusForbidden, gin.H{"error": "Not allowed for user"})
		c.Abort()
	}
}
