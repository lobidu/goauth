package middleware

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"goauth/common"
	"goauth/controller"
	"goauth/models"
	"goauth/services/tokenservice"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"os"
	"strings"
)

func ValidateLogin(c *gin.Context) {
	var input controller.LoginInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		c.Abort()
		return
	}

	var user models.User

	invalidErrorMessage := "Invalid Password or Email"

	if err := common.DB.Where("email = ?", input.Email).Preload("Permissions").
		First(&user).Error; err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": invalidErrorMessage})
		c.Abort()
		return
	}

	byteHash := []byte(user.PasswordHash)

	err := bcrypt.CompareHashAndPassword(byteHash, []byte(input.Password))
	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": invalidErrorMessage})
		c.Abort()
		return
	}
	c.Set("user", &user)
	c.Next()
}

func ValidateRefreshToken(c *gin.Context) {
	var input controller.RefreshInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		c.Abort()
		return
	}

	err, claim := tokenservice.ValidateRefreshToken(input.Token)

	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid Token"})
		c.Abort()
		return
	}

	var user models.User

	if err := common.DB.Where("id = ?", claim.UserId).Preload("Permissions").
		First(&user).Error; err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid User"})
		c.Abort()
		return
	}

	c.Set("user", &user)
	c.Next()
}

// Authenticate Authentication Middleware
func Authenticate(c *gin.Context) {
	var token string
	auth := c.Request.Header.Get("Authorization")
	if auth == "" {
		cookie, err := c.Request.Cookie("auth_token")

		if err != nil || cookie.Value == "" {
			c.String(http.StatusForbidden, "No Authorization token provided")
			c.Abort()
			return
		}

		if c.Request.Header.Get("Origin") != os.Getenv("FRONTEND_ORIGIN") {
			c.String(http.StatusForbidden,
				"Cookie Authorization not allowed for this Origin")
			c.Abort()
			return
		}
	} else {
		token = strings.TrimPrefix(auth, "Bearer ")
		if token == auth {
			c.String(http.StatusForbidden, "Could not find bearer token in Authorization header")
			c.Abort()
			return
		}
	}

	err, claim := tokenservice.ValidateAuthenticationToken(token)

	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid Token"})
		c.Abort()
		return
	}

	userId, err := uuid.Parse(claim.UserId)

	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid User ID"})
		c.Abort()
		return
	}

	c.Set("userId", userId)
	c.Set("permissions", claim.Permissions)

	c.Next()
}
