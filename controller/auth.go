package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"goauth/common"
	"goauth/models"
	"goauth/services/mailservice"
	"goauth/services/tokenservice"
	"goauth/services/userservice"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

type LoginInput struct {
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type RefreshInput struct {
	Token string `json:"token"`
}

type VerifyEmailInput struct {
	Token string `json:"token" form:"token" binding:"required"`
}

type PasswordUpdateInput struct {
	OldPassword string `json:"oldPassword" binding:"required"`
	Password    string `json:"password" binding:"required"`
}

type PasswordResetRequestInput struct {
	Email string `json:"email" binding:"required"`
}

type PasswordResetInput struct {
	UserID   string `json:"userID" binding:"required"`
	Token    string `json:"token" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type SendVerificationMailInput struct {
	Email string `json:"email" binding:"required"`
}

type RevokeTokenInput struct {
	Token string `json:"token" binding:"required"`
}

func GetTokens(c *gin.Context) {
	data, ok := c.Get("user")

	if !ok {
		c.JSON(http.StatusInternalServerError,
			gin.H{"error": "No User assigned to Request"})
		return
	}

	user, ok := data.(*models.User)

	if !ok {
		c.JSON(http.StatusInternalServerError,
			gin.H{"error": "Not a User object assigned to Request"})
		return
	}

	var permissionClaim []string

	common.DB.Model(&user).Association("Permissions").Find(&user.Permissions)

	for _, permission := range user.Permissions {
		if permission.ExpiresAt.IsZero() || permission.ExpiresAt.After(time.Now()) {
			permissionClaim = append(permissionClaim, permission.PermissionName)
		}
	}

	err, authToken := tokenservice.GenerateAuthenticationToken(
		user.ID.String(),
		permissionClaim,
	)

	if err != nil {
		c.JSON(http.StatusInternalServerError,
			gin.H{"error": "Unable to generate auth token"})
		return
	}

	err, refreshToken := tokenservice.GenerateRefreshToken(
		user.ID.String(),
	)

	if err != nil {
		c.JSON(http.StatusInternalServerError,
			gin.H{"error": "Unable to generate Refresh token"})
		return
	}

	_, idToken := tokenservice.GenerateIdToken(user)

	c.JSON(http.StatusOK, gin.H{
		"auth_token":    authToken,
		"id_token":      idToken,
		"refresh_token": refreshToken,
	})
}

func RequestPasswordReset(c *gin.Context) {
	var input PasswordResetRequestInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var user models.User

	if err := common.DB.Where("email = ?", input.Email).First(&user).Error; err != nil {
		// Do not expose the fact whether an email exists to an unauthorized
		// user
		c.JSON(http.StatusOK, gin.H{"data": true})
		return
	}

	verificationCode := common.GenerateRandomString(12)
	hashPayload := fmt.Sprintf("%s.%s", user.ID.String(), verificationCode)
	verificationCodeHash, err := bcrypt.GenerateFromPassword(
		[]byte(hashPayload),
		bcrypt.DefaultCost,
	)
	if err != nil {
		log.Printf("Unable to generate Hash from PW reset code: %s", err)
	}

	code := models.VerificationCode{
		CodeHash: string(verificationCodeHash),
	}

	common.DB.Create(&code)

	token := fmt.Sprintf("%d.%s", code.ID, verificationCode)

	go mailservice.SendPasswordResetMail(mailservice.PasswordResetMail{
		User:              &user,
		VerificationToken: token,
	})

	c.JSON(http.StatusOK, gin.H{"data": true})
}

func VerifyEmail(c *gin.Context) {
	input := &VerifyEmailInput{}

	if err := c.ShouldBindQuery(input); err != nil {
		c.String(http.StatusBadRequest, err.Error())
		return
	}

	tokenParts := strings.Split(input.Token, ".")

	tokenID, err := strconv.ParseInt(tokenParts[0], 10, 64)

	if err != nil {
		c.String(http.StatusBadRequest, "Invalid Token")
		return
	}

	code := tokenParts[1]

	var verificationCode models.VerificationCode

	if err := common.DB.Joins("User").First(&verificationCode, tokenID).
		Error; err != nil {
		c.String(http.StatusBadRequest, "Invalid Token ID")
		return
	}

	byteHash := []byte(verificationCode.CodeHash)
	hashPayload := fmt.Sprintf("%s.%s", verificationCode.User.Email, code)

	err = bcrypt.CompareHashAndPassword(byteHash, []byte(hashPayload))

	if err != nil {
		c.String(http.StatusBadRequest, "Invalid Code")
		return
	}

	common.DB.Model(verificationCode.User).Updates(models.User{EmailVerified: true})
	common.DB.Delete(&verificationCode)

	c.Redirect(http.StatusFound, os.Getenv("REDIRECT_AFTER_VERIFICATION"))
}

func UpdatePassword(c *gin.Context) {
	var input PasswordUpdateInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user, err := userservice.GetUserFromContext(c)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	byteHash := []byte(user.PasswordHash)

	err = bcrypt.CompareHashAndPassword(byteHash, []byte(input.OldPassword))
	if err != nil {
		c.JSON(http.StatusBadRequest,
			gin.H{"error": "Old Password is incorrect"})
		return
	}

	newHash, err := bcrypt.GenerateFromPassword(
		[]byte(input.Password),
		bcrypt.DefaultCost,
	)

	common.DB.Model(&user).Update("PasswordHash", newHash)

	c.JSON(http.StatusOK, gin.H{"data": true})

}

func ResetPassword(c *gin.Context) {
	var input PasswordResetInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	tokenParts := strings.Split(input.Token, ".")

	tokenID := tokenParts[0]
	code := tokenParts[1]

	var verificationCode models.VerificationCode

	if err := common.DB.Where("id = ?", tokenID).First(&verificationCode).
		Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid Token ID"})
		return
	}

	byteHash := []byte(verificationCode.CodeHash)
	hashPayload := fmt.Sprintf("%s.%s", input.UserID, code)

	err := bcrypt.CompareHashAndPassword(byteHash, []byte(hashPayload))

	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid Code or Email"})
		return
	}

	var user models.User

	if err := common.DB.Where("id = ?", input.UserID).First(&user).
		Error; err != nil {
		c.JSON(http.StatusBadRequest,
			gin.H{"error": "Invalid User ID"})
		return
	}

	newPasswordHash, err := bcrypt.GenerateFromPassword(
		[]byte(input.Password),
		bcrypt.DefaultCost,
	)

	if err != nil {
		log.Println(err)
	}

	common.DB.Model(&user).Updates(models.User{PasswordHash: string(newPasswordHash)})
	common.DB.Delete(&verificationCode)

	c.JSON(http.StatusOK, gin.H{"data": true})
}

func SendVerificationMail(c *gin.Context) {
	var input SendVerificationMailInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var user models.User

	if err := common.DB.Where("email = ?", input.Email).First(&user).
		Error; err != nil {
		c.JSON(http.StatusBadRequest,
			gin.H{"error": "This Email is no longer assigned to a User"})
		return
	}

	userservice.VerifyEmail(&user)

	c.JSON(http.StatusOK, gin.H{"data": true})
}

func GetPublicKey(c *gin.Context) {
	key, err := os.ReadFile(os.Getenv("JWT_SIGNING_PUBLIC_KEY_PATH"))

	if err != nil {
		c.JSON(http.StatusInternalServerError,
			gin.H{"error": "Error retrieving Public Key"})
		return
	}

	c.String(http.StatusOK, string(key))
}

func RevokeToken(c *gin.Context) {
	var input RevokeTokenInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err, claims := tokenservice.ValidateTokenToBeRevoked(input.Token)

	if err != nil {
		c.JSON(http.StatusBadRequest,
			gin.H{"error": "Cannot revoke invalid Token"})
		return
	}

	userId, ok := c.Get("userId")

	if !ok {
		c.JSON(http.StatusInternalServerError,
			gin.H{"error": "No User in Context"})
		return
	}

	tokenUserId, err := uuid.Parse(claims.UserId)

	if err != nil {
		c.JSON(http.StatusBadRequest,
			gin.H{"error": "Token has an invalid User ID"})
		return
	}

	// TODO: Allow users with permission `delete:tokens` to delete anyway
	if userId != tokenUserId {
		c.JSON(http.StatusUnauthorized,
			gin.H{"error": "Can only revoke own tokens"})
		return
	}

	tokenID, err := uuid.Parse(claims.ID)

	if err != nil {
		c.JSON(http.StatusBadRequest,
			gin.H{"error": "Token has an invalid ID"})
		return
	}

	code := models.RevokedTokens{
		TokenID:     tokenID,
		TokenExpiry: claims.ExpiresAt.UTC(),
	}

	err = common.DB.Create(&code).Error

	if err != nil {
		c.JSON(http.StatusInternalServerError,
			gin.H{"error": "Unable to Store Revoked token"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"result": "ok"})
}
