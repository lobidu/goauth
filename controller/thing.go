package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func FindThing(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"data": "thing"})
}

func CreateThing(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"data": "thing"})
}

func UpdateThing(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"data": "thing"})
}

func DeleteThing(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"data": "thing"})
}
