package controller

import (
	"errors"
	"github.com/fatih/structs"
	"github.com/gin-gonic/gin"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgerrcode"
	"goauth/common"
	"goauth/models"
	"goauth/services/userservice"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm/clause"
	"log"
	"net/http"
)

type CreateUserInput struct {
	Email       string `json:"email" binding:"required"`
	Password    string `json:"password" binding:"required"`
	TosAccepted bool   `json:"tosAccepted" binding:"required"`
}

type UpdateUserInput struct {
	Email string `json:"email"`
}

func FindUser(c *gin.Context) {
	user, err := userservice.FindById(c.Param("id"))

	if err != nil {
		c.JSON(http.StatusBadRequest,
			gin.H{"error": "Invalid User ID"})
		return
	}

	if currentUserId, _ := c.Get("userId"); user.ID != currentUserId {
		c.JSON(http.StatusForbidden,
			gin.H{"error": "Cannot view another user"})
		return
	}

	c.JSON(http.StatusOK, user)
}

func CreateUser(c *gin.Context) {
	// Validate input
	var input CreateUserInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if !input.TosAccepted {
		c.JSON(http.StatusBadRequest,
			gin.H{"error": "Terms of Service must be accepted"})
		return
	}

	hash, err := bcrypt.GenerateFromPassword(
		[]byte(input.Password),
		bcrypt.DefaultCost,
	)

	if err != nil {
		log.Println(err)
		c.JSON(http.StatusInternalServerError,
			gin.H{"error": "Error during Password hashing"})
		return
	}

	user := &models.User{
		Email:        input.Email,
		Name:         input.Email,
		PasswordHash: string(hash),
		TosAccepted:  input.TosAccepted,
		Permissions: []models.UserPermission{
			{PermissionName: "create:thing"},
			{PermissionName: "read:thing"},
		},
	}

	if err := common.DB.Create(user).Error; err != nil {
		if pgError := err.(*pgconn.PgError); errors.Is(err,
			pgError) && pgError.Code == pgerrcode.UniqueViolation {
			c.JSON(http.StatusBadRequest,
				gin.H{"error": "An Account with that email address already exists"})
			return
		}
		log.Println(err)
		c.JSON(http.StatusInternalServerError,
			gin.H{"error": "An Internal Server Error has occurred during User creation"})
		return
	}

	go userservice.VerifyEmail(user)

	c.JSON(http.StatusOK, gin.H{"data": user})
}

func UpdateUser(c *gin.Context) {
	// Validate input
	var input UpdateUserInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user, err := userservice.FindById(c.Param("id"))

	if err != nil {
		c.JSON(http.StatusBadRequest,
			gin.H{"error": "Invalid User ID"})
		return
	}

	if currentUserId, _ := c.Get("userId"); user.ID != currentUserId {
		c.JSON(http.StatusForbidden,
			gin.H{"error": "Cannot view another user"})
		return
	}

	updatedUser := structs.Map(&input)

	if input.Email != user.Email {
		updatedUser["emailVerified"] = false
		go userservice.VerifyEmail(user)
	}

	common.DB.Model(&user).Updates(updatedUser)

	c.JSON(http.StatusOK, gin.H{"data": user})
}

func DeleteUser(c *gin.Context) {
	user, err := userservice.FindById(c.Param("id"))

	if err != nil {
		c.JSON(http.StatusBadRequest,
			gin.H{"error": "Invalid User ID"})
		return
	}

	if currentUserId, _ := c.Get("userId"); user.ID != currentUserId {
		c.JSON(http.StatusForbidden,
			gin.H{"error": "Cannot delete another user"})
		return
	}

	result := common.DB.Select(clause.Associations).Delete(&user)

	if result.Error != nil {
		log.Println(err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Unable to delete the user in the Database."})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": true})
}
