package common

import (
	"fmt"
	"goauth/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
)

func getDsn() string {
	return fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=disable",
		os.Getenv("POSTGRES_HOST"),
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		os.Getenv("POSTGRES_DB"),
		os.Getenv("POSTGRES_PORT"),
	)
}

func getDb() (*gorm.DB, error) {
	return gorm.Open(postgres.Open(getDsn()), &gorm.Config{})
}

var DB *gorm.DB

func SetupDB() error {

	db, err := getDb()
	if err != nil {
		return err
	}

	err = db.AutoMigrate(
		&models.Permission{},
		&models.User{},
		&models.UserPermission{},
		&models.VerificationCode{},
		&models.RevokedTokens{},
	)

	if err != nil {
		return err
	}

	DB = db

	return nil
}
