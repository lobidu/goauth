package main

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"goauth/common"
	"goauth/router"
	"log"
	"net/http"
	"os"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	err = common.SetupDB()

	if err != nil {
		log.Fatalf("Error connecting the Database: %s\n", err)
	}

	engine := gin.Default()

	engine.Use(gin.Logger())

	engine.Use(func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", os.Getenv("FRONTEND_URL"))
	})

	err = engine.SetTrustedProxies(nil)

	if err != nil {
		log.Fatalf("Error setting trusted Proxies: %s\n", err)
	}

	router.RegisterRoutes(engine)

	err = engine.Run(fmt.Sprintf(":%s", os.Getenv("PORT")))

	if errors.Is(err, http.ErrServerClosed) {
		fmt.Printf("server closed\n")
	} else if err != nil {
		log.Fatalf("error starting server: %s\n", err)
	}
}
